package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class homePage {

    WebDriver driver;

    By btnAleatoir = By.xpath("//*[@id='tabvanilla']//*[text()='Aléatoire']");


    public homePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isAleatoirButtonVisible() {
        return driver.findElement(btnAleatoir).isDisplayed();

    }

    public void clickOnAleatoirButton() {
        driver.findElement(btnAleatoir).click();
    }


}
