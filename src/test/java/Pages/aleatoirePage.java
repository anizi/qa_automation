package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class aleatoirePage {

    WebDriver driver;
    By facebookWidget = By.xpath('//*[@src="https://bouletcorp.com/wp-content/plugins/socialize-this/widgets/alteredicons/facebook.png"]');
    By twitterWidget = By.xpath('//*[@src="https://bouletcorp.com/wp-content/plugins/socialize-this/widgets/alteredicons/twitter.png"]');
    By tumblrWidget = By.xpath('//*[@src="https://bouletcorp.com/wp-content/themes/bouletcorp/images/icons/Tumblr.png"]');

    public aleatoirePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isFacebookWidgetDisplayed() {
        return driver.findElement(facebookWidget).isDisplayed();
    }

    public boolean isTwitterWidgetDisplayed() {
        return driver.findElement(twitterWidget).isDisplayed();
    }

    public boolean isTumblrWidgetDisplayed() {
        return driver.findElement(tumblrWidget).isDisplayed();
    }
}