package StepDef;

import Pages.aleatoirePage;
import Pages.homePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class BoulletcorpStepDef {


    homePage HomePage;
    aleatoirePage AleatoirePage;
    WebDriver driver;

    // Il vaut mieux ajouter un package config qui contient un fichier .proprities qui contient le config
// (URL, browser, driver, db connection config, timeout ... ) et un dataFileReader pour lire la config
    @Given("as a user I surrender on Bouletcorp site")
    public void goToBouletcorpsSite() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\anizi\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("http://www.bouletcorp.com");

    }

    @When("i click on Aléatoire")
    public void clickOnAleatoire() {
        Assert.assertTrue("Aléatoir button is not visible", HomePage.isAleatoirButtonVisible());
        HomePage.clickOnAleatoirButton();
    }

    @Then("the page change")
    public void checkChangedPage() {
        Assert.assertFalse("page not changed", driver.getCurrentUrl().equals("https://bouletcorp.com"));
    }

    @Then("the widgets facebook, twitter and tumblr are displayed")
    public void checkWidgets() {
        Assert.assertTrue("Facebook widget is not displayed", AleatoirePage.isFacebookWidgetDisplayed());
        Assert.assertTrue("Twitter widget is not displayed", AleatoirePage.isTwitterWidgetDisplayed());
        Assert.assertTrue("Tumblr widget is not displayed", AleatoirePage.isTumblrWidgetDisplayed());
    }
}
