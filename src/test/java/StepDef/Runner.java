package StepDef;




import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Boletcorp", glue = {"StepDef"}, monochrome = true)
public class Runner {
    //on peut ajouter des plugin içi ( reporting, screenshots ... ) or tag
}
